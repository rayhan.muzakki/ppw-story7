from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from selenium import webdriver
from django.urls import resolve
from django.apps import apps
from home.apps import HomepageConfig
from .forms import *
from .views import *
from .models import *
import unittest
import time

# Create your tests here.
class unitTest(TestCase):
    def test_idxurl(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_confirmationurl(self):
        response = Client().get('/confirmation/')
        self.assertEqual(response.status_code,200)
#template

    def test_indextemplate(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_confirmationtemplate(self):
        response = Client().get('/confirmation/')
        self.assertTemplateUsed(response, 'confirmation.html')
#function

    def test_funcindex(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_funcconfirmation(self):
        found = resolve('/confirmation/')
        self.assertEqual(found.func, confirmation)

#create model

    def test_createnewstatus(self):
        new = Status.objects.create(name = 'Some', status = 'Hello')
        self.assertTrue(isinstance(new, Status))
        self.assertTrue(new.__str__(), new.name)
        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)

    def test_displaystatus(self):
        status = "Hello"
        post_data = Client().post('/', {'stat' : status})
        self.assertEqual(post_data.status_code, 200)

#validation
    
    def test_index_valid(self):
        response = self.client.post('', data={'name':'Ae', 'status':'Nice'})
        response_content = response.content.decode()
        self.assertNotIn(response_content, 'Ae')
        self.assertNotIn(response_content, 'Nice')

    def test_confirmation_valid(self):
        response = self.client.post('/confirmation/', data={'name':'Ae', 'status':'Nice'})
        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)
        self.assertEqual(response.status_code, 302)
        response_content = response.content.decode()
        self.assertIn(response_content, 'Ae')
        self.assertIn(response_content, 'Nice')

#====================================================================================

class functionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')


    def tearDown(self):
        self.driver.quit()

    def test_submit_yes_story7(self):
        self.driver.get(self.live_server_url)
        time.sleep(3)
        name_box = self.driver.find_element_by_name('name')
        name_box.send_keys('Some')
        status_box = self.driver.find_element_by_name('status')
        status_box.send_keys('Happy')
        submit_button = self.driver.find_element_by_name('submit')
        submit_button.click()
        time.sleep(3)
        save_button = self.driver.find_element_by_name('save')
        save_button.click()
        time.sleep(3)
        self.assertIn('Some', self.driver.page_source)
        self.assertIn('Happy', self.driver.page_source)

    def test_submit_no_story7(self):
        self.driver.get(self.live_server_url)
        time.sleep(3)
        name_box = self.driver.find_element_by_name('name')
        name_box.send_keys('Yoe')
        status_box = self.driver.find_element_by_name('status')
        status_box.send_keys('uwu')
        submit_button = self.driver.find_element_by_name('submit')
        submit_button.click()
        time.sleep(3)
        save_button = self.driver.find_element_by_name('back')
        save_button.click()
        time.sleep(3)
        self.assertNotIn('Yoe', self.driver.page_source)
        self.assertNotIn('uwu', self.driver.page_source)